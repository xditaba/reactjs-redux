const redux = require('redux');
const createStore = redux.createStore;
const initialState = {
  counter: 0,
};

// Reducer
const rootReducer = (state = initialState, action) => {
  console.log('--- action print ---');
  console.log(action);
  switch (action.type) {
    case 'INC_COUNTER':
      state = {
        ...state,
        counter: state.counter + 1,
      };
      break;
    case 'ADD_COUNTER':
      state = {
        ...state,
        counter: state.counter + action.value,
      };
      break;
    default:
  }
  return state;
};

// Store
const store = createStore(rootReducer);
console.log(store.getState());

// Subscription
store.subscribe(() => {
  console.log('[Subcription]', store.getState());
});

// Dispatching Action
store.dispatch({ type: 'INC_COUNTER' });
store.dispatch({ type: 'ADD_COUNTER', value: 10 });

console.log('--- store ---');
console.log(store);
// console.log(store.dispatch.toString());
console.log('--- redux ---');
console.log(redux);
console.log('--- Final State ---');
console.log(store.getState());
